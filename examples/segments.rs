//! Use the chip to display letters and numbers on some 14 segment display

#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use panic_halt as _;
use embassy_time::Timer;
use embassy_rp::{i2c, bind_interrupts, gpio};
use embassy_rp::peripherals::I2C1;
use ht16k33_async::HT16K33;

bind_interrupts!(struct Irqs {
    I2C1_IRQ => i2c::InterruptHandler<I2C1>;
});


// You will need to change these according to your wiring.
// The letter and number definitions should compute a font from these
const SEG_A: u16 =  0b0010_0000_0000_0000;
const SEG_B: u16 =  0b0100_0000_0000_0000;
const SEG_C: u16 =  0b0000_0000_1000_0000;
const SEG_D: u16 =  0b0000_0000_0010_0000;
const SEG_E: u16 =  0b0000_0000_0000_0001;
const SEG_F: u16 =  0b0000_0001_0000_0000;
const SEG_G1: u16 = 0b0001_0000_0000_0000;
const SEG_G2: u16 = 0b0000_0000_0001_0000;
const SEG_H: u16 =  0b0000_0010_0000_0000;
const SEG_J: u16 =  0b0000_0100_0000_0000;
const SEG_K: u16 =  0b0000_1000_0000_0000;
const SEG_L: u16 =  0b0000_0000_0000_1000;
const SEG_M: u16 =  0b0000_0000_0000_0100;
const SEG_N: u16 =  0b0000_0000_0000_0010;
const SEG_DP: u16 = 0b0000_0000_0100_0000;

const LETTER_A: u16 = SEG_A | SEG_B | SEG_C | SEG_E | SEG_F | SEG_G1 | SEG_G2;
const LETTER_B: u16 = SEG_A | SEG_B | SEG_C | SEG_D | SEG_G2 | SEG_J | SEG_M;
const LETTER_C: u16 = SEG_A | SEG_D | SEG_E | SEG_F;
const LETTER_D: u16 = SEG_A | SEG_B | SEG_C | SEG_D | SEG_J | SEG_M;
const LETTER_E: u16 = SEG_A | SEG_D | SEG_E | SEG_F | SEG_G1 | SEG_G2;
const LETTER_F: u16 = SEG_A | SEG_E | SEG_F | SEG_G1;
const LETTER_G: u16 = SEG_A | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G2;
const LETTER_H: u16 = SEG_B | SEG_C | SEG_E | SEG_F | SEG_G1 | SEG_G2;
const LETTER_I: u16 = SEG_A | SEG_D | SEG_J | SEG_M;
const LETTER_J: u16 = SEG_B | SEG_C | SEG_D | SEG_E;
const LETTER_K: u16 = SEG_E | SEG_F | SEG_G1 | SEG_K | SEG_L;
const LETTER_L: u16 = SEG_D | SEG_E | SEG_F;
const LETTER_M: u16 = SEG_B | SEG_C | SEG_E | SEG_F | SEG_H | SEG_K;
const LETTER_N: u16 = SEG_B | SEG_C | SEG_E | SEG_F | SEG_H | SEG_L;
const LETTER_O: u16 = SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F;
const LETTER_P: u16 = SEG_A | SEG_B | SEG_E | SEG_F | SEG_G1 | SEG_G2;
const LETTER_Q: u16 = SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_L;
const LETTER_R: u16 = SEG_A | SEG_B | SEG_E | SEG_F | SEG_G1 | SEG_G2 | SEG_L;
const LETTER_S: u16 = SEG_A | SEG_C | SEG_D | SEG_F | SEG_G1 | SEG_G2;
const LETTER_T: u16 = SEG_A | SEG_J | SEG_M;
const LETTER_U: u16 = SEG_B | SEG_C | SEG_D | SEG_E | SEG_F;
const LETTER_V: u16 = SEG_E | SEG_F | SEG_K | SEG_N;
const LETTER_W: u16 = SEG_B | SEG_C | SEG_E | SEG_F | SEG_L | SEG_N;
const LETTER_X: u16 = SEG_H | SEG_K | SEG_L | SEG_N;
const LETTER_Y: u16 = SEG_H | SEG_K | SEG_M;
const LETTER_Z: u16 = SEG_A | SEG_D | SEG_K | SEG_N | SEG_G1 | SEG_G2;

const NUMBERS: [u16; 10] = [
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_K | SEG_N,  // 0
    SEG_B | SEG_C | SEG_K,  // 1
    SEG_A | SEG_B | SEG_D | SEG_E | SEG_G1 | SEG_G2,  // 2
    SEG_A | SEG_B | SEG_C | SEG_D | SEG_G2,  // 3
    SEG_B | SEG_C | SEG_F | SEG_G1 | SEG_G2,  // 4
    LETTER_S,  // 5
    LETTER_S | SEG_E,  // 6
    SEG_A | SEG_B | SEG_C,  // 7
    LETTER_O | SEG_G1 | SEG_G2,  // 8
    LETTER_S | SEG_B,  // 9
];


const fn split(num: u16) -> [u8; 2] {
    [(num & 0xFF) as u8, ((num >> 8) & 0xFF) as u8]
}


#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let mut led = gpio::Output::new(p.PIN_25, gpio::Level::Low);

    let scl = p.PIN_19;
    let sda = p.PIN_18;
    let i2c = i2c::I2c::new_async(p.I2C1, scl, sda, Irqs, i2c::Config::default());
    let mut driver = HT16K33::new(i2c, 0x70);

    driver.setup().await.unwrap();

    led.set_high();

    loop {
        driver.write_to_common(0, &split(LETTER_A)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_B)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_C)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_D)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_E)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_F)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_G)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_H)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_I)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_J)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_K)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_L)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_M)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_N)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_O)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_P)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_Q)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_R)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_S)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_T)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_U)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_V)).await.unwrap();
        driver.write_to_common(2, &split(LETTER_W)).await.unwrap();
        driver.write_to_common(3, &split(LETTER_X)).await.unwrap();

        Timer::after_secs(1).await;

        driver.write_to_common(0, &split(LETTER_Y)).await.unwrap();
        driver.write_to_common(1, &split(LETTER_Z)).await.unwrap();
        driver.write_to_common(2, &split(SEG_DP)).await.unwrap();
        driver.write_to_common(3, &split(SEG_DP)).await.unwrap();

        Timer::after_secs(1).await;

        for num in NUMBERS {
            driver.write_to_common(0, &split(num)).await.unwrap();
            driver.write_to_common(1, &split(num)).await.unwrap();
            driver.write_to_common(2, &split(num)).await.unwrap();
            driver.write_to_common(3, &split(num)).await.unwrap();

            Timer::after_millis(1000).await;
        }
    }
}
