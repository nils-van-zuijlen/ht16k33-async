#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use embassy_executor::Spawner;
use panic_halt as _;
use embassy_time::Timer;
use embassy_rp::{i2c, bind_interrupts, gpio};
use embassy_rp::peripherals::I2C1;
use ht16k33_async::HT16K33;

bind_interrupts!(struct Irqs {
    I2C1_IRQ => i2c::InterruptHandler<I2C1>;
});


#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let mut led = gpio::Output::new(p.PIN_25, gpio::Level::Low);

    let scl = p.PIN_19;
    let sda = p.PIN_18;
    let i2c = i2c::I2c::new_async(p.I2C1, scl, sda, Irqs, i2c::Config::default());
    let mut driver = HT16K33::new(i2c, 0x70);

    driver.setup().await.unwrap();

    led.set_high();

    let mut buffer = [0u8; 2*4];

    loop {
        for c in 0..8 {
            for l in 0..8 {
                buffer[c] ^= 1 << l;

                driver.write_whole_display(&buffer).await.unwrap();

                Timer::after_millis(50).await;
            }
        }
    }
}
